current_dir = File.dirname(__FILE__)

log_level                :info
log_location             STDOUT
node_name                "services-db-152"
client_key               "#{current_dir}/services-db-152.pem"
validation_client_name   "eucon-validator"
validation_key           "#{current_dir}/eucon-validator.pem"
chef_server_url          "https://eis-ms-db-152.ms.eucon.local/organizations/eucon"
cookbook_path            ["#{current_dir}/../cookbooks"]
