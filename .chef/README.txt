chef server installieren:
1. kopieren deb
2. !!! DANACH in Einstellungen Netzwerk Port-Weiterleitung hizufügen: ssl mit alles wie bei ssh aber port beide Seite 443
3. wenn hostname in VB und link zu verbinden nicht gleich sind.. hab hostname in VB verändert als 127.0.0.1 (hostnamectl set-hostname new-hostname), danach "vagrant reload" und neu cert bekommen mit 
   "knife ssl fetch"
   !!! das muss unbedingt vor chef server installierung sein
4. sudo dpkg -i chef-server-core_*.deb
5. sudo chef-server-ctl reconfigure
6. sudo chef-server-ctl user-create USERNAME FIRST_NAME LAST_NAME EMAIL PASSWORD 
  sudo chef-server-ctl user-create admin admin admin admin@example.com examplepass -f admin.pem
  sudo chef-server-ctl user-create vagrant vagrant vagrant vagrant@example.com examplepass -f vagrant.pem
  # hab nächste genomen
  sudo chef-server-ctl user-create vagrant vagrant vagrant Natalia.Nast@eucon.com vagrant -f vagrant.pem
7.  chef-server-ctl org-create SHORTNAME LONGNAME --association_user USERNAME
  example: sudo chef-server-ctl org-create digitalocean "DigitalOcean, Inc." --association_user admin -f digitalocean-validator.pem
  #hab folgende genommen:
  sudo chef-server-ctl org-create eucon 'Eucon GmbH' --association_user vagrant -f eucon-validator.pem
8. knife muss so aussehen:
	current_dir = File.dirname(__FILE__)
	
	log_level                :info
	log_location             STDOUT
	node_name                "vagrant"
	client_key               "#{current_dir}/vagrant.pem"
	validation_client_name   "eucon-validator"
	validation_key           "#{current_dir}/eucon-validator.pem"
	chef_server_url          "https://127.0.0.1/organizations/eucon"
	cookbook_path            ["#{current_dir}/../cookbooks"]
9. jetzt prüfen ob läuft: knife client list
