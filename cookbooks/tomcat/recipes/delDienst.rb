﻿#
# Cookbook Name:: tomcat
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

cnt = "#{node['tomcat']['count']}".to_i
i = 0
puts "cnt = #{cnt} und i = #{i}"

while i < cnt do

  increment = ("#{node['tomcat']['INCREMENT']}".to_i)*i
  puts "#{increment}"

  ####### ports + increment
  port          = "#{node['tomcat']['port']}".to_i + increment
  ####### und ServiceName auch
  SERVICE_NAME  = "#{node['tomcat']['SERVICE_NAME']}-#{port}"
  
  ##########################################

  service "#{SERVICE_NAME}" do
    action :stop
    only_if "sc query #{SERVICE_NAME}"
  end
  
  execute "Dienst_löschen" do
    command "sc delete #{SERVICE_NAME}"
    only_if "sc query #{SERVICE_NAME}"
  end
  
  i = i + 1
  
end