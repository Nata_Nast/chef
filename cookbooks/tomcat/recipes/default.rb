﻿#
# Cookbook Name:: tomcat
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

document_root = ""
node['tomcat']['folder'].each do |attribute, folder_name|
  document_root = "#{document_root}/" + folder_name["name"]
  puts "#{document_root}"
  directory "#{document_root}" do
    owner   'root'
    group   'root'
    action  :create
    not_if do ::File.exists?("#{document_root}") end
  end
end

cnt = "#{node['tomcat']['count']}".to_i
i = 0
puts "cnt = #{cnt} und i = #{i}"

while i < cnt do

  increment = ("#{node['tomcat']['INCREMENT']}".to_i)*i
  puts "#{increment}"

  ####### ports + increment
  port          = "#{node['tomcat']['port']}".to_i + increment
  ssl_port      = "#{node['tomcat']['ssl_port']}".to_i + increment
  ajp_port      = "#{node['tomcat']['ajp_port']}".to_i + increment
  shutdown_port = "#{node['tomcat']['shutdown_port']}".to_i + increment
  JMX_PORT      = "#{node['tomcat']['JMX_PORT']}".to_i + increment
  ####### und ServiceName auch
  SERVICE_NAME  = "#{node['tomcat']['SERVICE_NAME']}-#{port}"
  
  ####### bekommen home folder
  port_folder = "#{node['tomcat']['base']}-#{port}"
  home = "#{document_root}/" + port_folder
  
  ############################
  
  puts "#{home}"
  directory home do
    owner   'root'
    group   'root'
    action  :create
    not_if do ::File.exists?("#{home}") end
  end
  
  ##########################################
  
  remote_directory home do 
    source "#{node['tomcat']['base']}"
    action :create
    not_if do ::File.exists?("#{home}/lib") end
  end
  
  template "#{home}/conf/server.xml" do
  	source "server.xml.erb"
  	mode "0644"
  	variables(
  		:port           => "#{port}",
  		:ssl_port       => "#{ssl_port}",
      :ajp_port       => "#{ajp_port}",
      :shutdown_port  => "#{shutdown_port}"
  	)
  end

  folder = "c:"+"\\"
  node['tomcat']['folder'].each do |attribute, folder_name|
    puts "#{document_root}"
    folder = "#{folder}\\"+folder_name["name"]
  end
  folder = "#{folder}\\" + port_folder
  
  template "#{home}/euconsetup/setenvcommon.bat" do
  	source "setenvcommon.bat.erb"
  	mode "0644"
  	variables(
      :port           => "#{port}",
  		:tc_home        => "#{folder}",
  		:JMX_PORT       => "#{JMX_PORT}",
      :SERVICE_NAME   => "#{SERVICE_NAME}"
  	)
  end
  
  execute "Dienst_tomcat" do
    cwd     "#{home}/euconsetup/64bit"
    command "InstallTomcatDaemon.cmd"
    not_if "sc query #{SERVICE_NAME}"
  end
  
  batch "jvm_opts" do
    code <<-EOH
    #{folder}\\bin\\#{SERVICE_NAME}.exe //US//#{SERVICE_NAME} ++JvmOptions "-XX:MaxPermSize=128m" ++JvmOptions "-Dfile.encoding=UTF-8" ++JvmOptions "-Deuconenvironment=production" ++JvmOptions "-Deuconrebuildconfig=false"
    EOH
  end

  service "#{SERVICE_NAME}" do
    supports :status => true, :restart => true
    subscribes :restart, resources(:template => "#{home}/conf/server.xml")
    action [ :enable, :start ]
  end
  
  i = i + 1
  
end