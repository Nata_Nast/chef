﻿#
# Cookbook Name:: tomcat
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

document_root = ""
node['tomcat']['folder'].each do |attribute, folder_name|
  document_root = "#{document_root}/" + folder_name["name"]
  puts "#{document_root}"
#  directory "#{document_root}" do
#    owner   'root'
#    group   'root'
#    action  :create
#    not_if do ::File.exists?("#{document_root}") end
#  end
end

cnt = "#{node['tomcat']['count']}".to_i
i = 0
puts "cnt = #{cnt} und i = #{i}"

while i < cnt do

  increment = ("#{node['tomcat']['INCREMENT']}".to_i)*i
  puts "#{increment}"

  ####### ports + increment
  port          = "#{node['tomcat']['port']}".to_i + increment
  ssl_port      = "#{node['tomcat']['ssl_port']}".to_i + increment
  ajp_port      = "#{node['tomcat']['ajp_port']}".to_i + increment
  shutdown_port = "#{node['tomcat']['shutdown_port']}".to_i + increment
  JMX_PORT      = "#{node['tomcat']['JMX_PORT']}".to_i + increment
  ####### und ServiceName auch
  SERVICE_NAME  = "#{node['tomcat']['SERVICE_NAME']}-#{port}"
  
  ####### bekommen home folder
  port_folder = "#{node['tomcat']['base']}-#{port}"
  home = "#{document_root}/" + port_folder
  
  ############################
 
  service "#{SERVICE_NAME}" do
    action :stop
    only_if "sc query #{SERVICE_NAME}"
  end
 
#  remote_directory home do 
#    source "#{node['tomcat']['base']}"
#    action :delete
#    only_if do ::File.exists?("#{home}") end
#  end
  
  puts "#{home}"
  remote_directory home do
#    owner   'root'
#    group   'root'
    action  :delete
#    only_if do ::File.exists?("#{home}") end
  end
  
  ##########################################
  
  execute "Dienst_löschen" do
    command "sc delete #{SERVICE_NAME}"
    only_if "sc query #{SERVICE_NAME}"
  end
  
  i = i + 1
  
end