@echo off
rem Licensed to the Apache Software Foundation (ASF) under one or more
rem contributor license agreements.  See the NOTICE file distributed with
rem this work for additional information regarding copyright ownership.
rem The ASF licenses this file to You under the Apache License, Version 2.0
rem (the "License"); you may not use this file except in compliance with
rem the License.  You may obtain a copy of the License at
rem
rem     http://www.apache.org/licenses/LICENSE-2.0
rem
rem Unless required by applicable law or agreed to in writing, software
rem distributed under the License is distributed on an "AS IS" BASIS,
rem WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
rem See the License for the specific language governing permissions and
rem limitations under the License.

if "%OS%" == "Windows_NT" setlocal
rem ---------------------------------------------------------------------------
rem NT Service Install/Uninstall script
rem
rem Options
rem install                Install the service using Tomcat6 as service name.
rem                        Service is installed using default settings.
rem remove                 Remove the service from the System.
rem
rem name        (optional) If the second argument is present it is considered
rem                        to be new service name                                           
rem
rem $Id: service.bat 908749 2010-02-10 23:26:42Z markt $
rem ---------------------------------------------------------------------------

rem Guess CATALINA_HOME if not defined
set "CURRENT_DIR=%cd%"
if not "%CATALINA_HOME%" == "" goto gotHome
set "CATALINA_HOME=%cd%"
if exist "%CATALINA_HOME%\bin\%SERVICE_NAME%.exe" goto okHome
rem CD to the upper dir
cd ..
set "CATALINA_HOME=%cd%"
:gotHome
if exist "%CATALINA_HOME%\bin\%SERVICE_NAME%.exe" goto okHome
echo The tomcat.exe was not found...
echo The CATALINA_HOME environment variable is not defined correctly.
echo This environment variable is needed to run this program
goto end
rem Make sure prerequisite environment variables are set
if not "%JAVA_HOME%" == "" goto okHome
echo The JAVA_HOME environment variable is not defined
echo This environment variable is needed to run this program
goto end 
:okHome
if not "%CATALINA_BASE%" == "" goto gotBase
set "CATALINA_BASE=%CATALINA_HOME%"
:gotBase
 
set "EXECUTABLE=%CATALINA_HOME%\bin\%SERVICE_NAME%.exe"

rem Set default Service name
set SERVICE_NAME=%SERVICE_NAME%
set PR_DISPLAYNAME=%SERVICE_NAME%

echo ### start service.bat %1 %2
if "%1" == "" goto displayUsage
:setServiceName
if %1 == install goto doInstall
if %1 == remove goto doRemove
if %1 == uninstall goto doRemove
echo Unknown parameter "%1"
:displayUsage
echo.
echo Usage: service.bat install/remove [service_name]
goto end

:doRemove
rem Remove the service
"%EXECUTABLE%" //DS//%SERVICE_NAME%
echo The service '%SERVICE_NAME%' has been removed
goto end

:doInstall
rem Install the service
echo Installing the service '%SERVICE_NAME%' ...
echo Using CATALINA_HOME:    "%CATALINA_HOME%"
echo Using CATALINA_BASE:    "%CATALINA_BASE%"
echo Using JAVA_HOME:        "%JAVA_HOME%"
echo Using JMX_PORT:         "%JMX_PORT%"
echo Using JVM_DLL: 		 "%jvm_dll%"
echo Using LOG_PATH: 		 "%log_path%"


rem Use the environment variables as an example
rem Each command line option is prefixed with PR_

set PR_DESCRIPTION=Apache Tomcat 7.0.56 Server - http://tomcat.apache.org/
echo ## PR_INSTALL=%EXECUTABLE%
set "PR_INSTALL=%EXECUTABLE%"
set "PR_LOGPATH=%log_path%"
set "PR_CLASSPATH=%CATALINA_BASE%\bin\tomcat-juli.jar;%CATALINA_HOME%\bin\tomcat-juli.jar;%CATALINA_HOME%\bin\bootstrap.jar;%JAVA_HOME%\lib\tools.jar"
set "jp=-Dcom.sun.management.jmxremote.port=%JMX_PORT%"
rem Set the server jvm from JAVA_HOME
set "PR_JVM=%jvm_dll%"
echo "%EXECUTABLE%" //IS//%SERVICE_NAME% --StartClass org.apache.catalina.startup.Bootstrap --StopClass org.apache.catalina.startup.Bootstrap --StartParams start --StopParams stop 
"%EXECUTABLE%" //IS//%SERVICE_NAME% --StartClass org.apache.catalina.startup.Bootstrap --StopClass org.apache.catalina.startup.Bootstrap --StartParams start --StopParams stop
if not errorlevel 1 goto installed
echo Failed installing '%SERVICE_NAME%' service
goto end
:installed
rem Set extra parameters
"%EXECUTABLE%" //US//%SERVICE_NAME% --JvmOptions "-Dcatalina.base=%CATALINA_BASE%;-Dcatalina.home=%CATALINA_HOME%;-Djava.endorsed.dirs=%CATALINA_HOME%\endorsed" --StartMode jvm --StopMode jvm --Startup=auto --Classpath=%JAVA_HOME%\lib\tools.jar;%PR_CLASSPATH%
rem More extra parameters
set PR_STDOUTPUT=%log_path%/stdout.log
set PR_STDERROR=%log_path%/stderr.log
"%EXECUTABLE%" //US//%SERVICE_NAME% ++JvmOptions "-Deucon_log_path=%log_path%;-Djava.io.tmpdir=%CATALINA_BASE%\temp;-Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager;-Djava.util.logging.config.file=%CATALINA_BASE%\conf\logging.properties" --Jvm %PR_JVM% --JvmMs %jvm_minheap% --JvmMx %jvm_maxheap%
"%EXECUTABLE%" //US//%SERVICE_NAME% ++JvmOptions "-Dcom.sun.management.jmxremote" ++JvmOptions "%jp%" ++JvmOptions "-Dcom.sun.management.jmxremote.ssl=false" ++JvmOptions "-Dcom.sun.management.jmxremote.authenticate=false" ++JvmOptions "-Djava.rmi.server.hostname=127.0.0.1"
"%EXECUTABLE%" //US//%SERVICE_NAME% ++JvmOptions "-XX:+HeapDumpOnOutOfMemoryError" ++JvmOptions "-XX:HeapDumpPath=%log_path%"
rem log4j aktivieren, wenn eine deployte App kein log4j.properties und kein log4j-jar mitbringt aber benötigt
rem "%EXECUTABLE%" //US//%SERVICE_NAME% ++JvmOptions "-D:log4j.configuration=%CATALINA_HOME%/shared/classes/log4j.properties"

echo The service '%SERVICE_NAME%' has been installed.
:end
cd "%CURRENT_DIR%"
