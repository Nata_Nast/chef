# 1. Konfiguration Server Ports 
	C:\EuconPrograms\apache-tomcat-7.0.62-eucon1-8080\conf\server.xml
# 2. Konfiguration Tomcat User
	C:\EuconPrograms\apache-tomcat-7.0.62-eucon1-8080\conf\tomcat-users.xml

# 3. Konfiguration f�r 64bit 
# 4. in C:\EuconPrograms\apache-tomcat-7.0.62-eucon1-8080\euconsetup\setenvcommon.bat
		a) Tomcat Home Verzeichnis (CATALINA_HOME)
		b) Windows Service Name
		c) Heap size
		d) Logpfad
		e) JMX Konfiguration mit Port
	
# 5. Konfiguration JAVA_HOME	
		C:\EuconPrograms\apache-tomcat-7.0.62-eucon1-8080\euconsetup\64bit\setenv.bat

# 6. Konfiguration Java f�r Windows Service
		C:\EuconPrograms\apache-tomcat-7.0.62-eucon1-8080\euconsetup\64bit\setenvDaemon.bat

# 7. Tomcat Installion:
   1. 64bit: ins Verzeichnis C:\EuconPrograms\apache-tomcat-7.0.62-eucon1-8080\euconsetup\64bit\ wechseln
   2. Das Skript InstallTomcatDaemon.cmd ausf�hren
   
Weitere Hinweise:
	Das Paket hat Jolokia (JMX �ber Rest) im Bauch. Der Aufrufpfad ist "jmx".

