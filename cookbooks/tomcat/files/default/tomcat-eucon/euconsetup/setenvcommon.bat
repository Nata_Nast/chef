set http_port=8080
set tc_version=apache-tomcat-7.0.62-eucon1-%http_port%
set tc_home=C:\EuconPrograms\%tc_version%
rem Windows Dienst name (Achtung auf Basis diesen Namens wird der Dienst vom Monitoring per SNMP �berwacht, bei Namens�nderungen muss das Monitoring angepasst werden)
set SERVICE_NAME=Tomcat7.0.62-%http_port%
set log_path=C:\EuconLogging\%SERVICE_NAME%
mkdir %log_path%

rem Max Java Heap Size 
set jvm_maxheap=512M
set jvm_minheap=256M

rem JMX Port setzen f�r Service- und Skript-Start
set JMX_PORT=8020
rem JMX aktivieren f�r Tomcat-Start �ber Skript (catalina.bat, startup.bat)
set CATALINA_OPTS=-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=%JMX_PORT% -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false
set CATALINA_HOME=%tc_home%