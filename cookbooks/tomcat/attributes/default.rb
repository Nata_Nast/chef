﻿# stellen welches increment TomcatPort haben muss: count*100 + port
default['tomcat']['count'] = 2

default['tomcat']['INCREMENT'] = 100
default['tomcat']['port'] = 8080 + 100
default['tomcat']['ssl_port'] = 8443 + 100
default['tomcat']['ajp_port'] = 8009 + 100
default['tomcat']['shutdown_port'] = 8005 + 100
default['tomcat']['JMX_PORT'] = 9020 + 100


#default['tomcat']['path'] = "/EuconPrograms/server/tomcat/nna"

############ REIHENFOLGE IST WICHTIG !!! ############
default['tomcat']['folder']['eucon_folder'] = {"name" => "EuconPrograms"}
default['tomcat']['folder']['server'] = {"name" => "server"}
default['tomcat']['folder']['tomcat'] = {"name" => "tomcat"}
#####################################################

default['tomcat']['base'] = "tomcat-eucon"
default['tomcat']['SERVICE_NAME'] = "#{node['tomcat']['base']}"
#default['tomcat']['version_folder'] = "#{node['tomcat']['eucon_folder']}/#{node['tomcat']['version']}"
#default['tomcat']['port_folder'] = ("#{node['tomcat']['port']}".to_i + "#{node['tomcat']['INCREMENT']}".to_i).to_s + "-#{node['tomcat']['base']}"
#default['tomcat']['home'] = "#{node['tomcat']['version_folder']}/" + "#{node['tomcat']['port_folder']}".to_s
default['tomcat']['resources'] = "/chef/chef-starter_db-152/chef-repo/cookbooks/tomcat/resources"
default['tomcat']['jvm_maxheap'] = "512M"
default['tomcat']['jvm_minheap'] = "256M"
