#
# Author:: Seth Chisamore (<schisamo@chef.io>)
# Cookbook Name:: sql_server
# Recipe:: default
#
# Copyright:: 2011, Chef Software, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#include_recipe 'sql_server::client'

distrib_path = "C:/sqlexpress"

directory distrib_path do
  recursive  true
  action    :create
  rights    :full_control, 'Jeder', :applies_to_children => true, :applies_to_self => true
  rights    :full_control, 'ERSTELLER-BESITZER', :applies_to_children => true, :applies_to_self => true
  rights    :full_control, 'root', :applies_to_children => true, :applies_to_self => true
  rights    :full_control, 'Benutzer', :applies_to_children => true, :applies_to_self => true
  inherits   true
  not_if    "sc query MSSQL$SQLEXPRESS"
  #do ::File.exists?("C:/sqlexpress") end
end

remote_directory "#{distrib_path}/SQL_Server_2008_R2_SP2_Express_Edition" do 
  source "SQL_Server_2008_R2_SP2_Express_Edition"
  recursive  true
  action    :create
  rights    :full_control, 'Jeder', :applies_to_children => true, :applies_to_self => true
  rights    :full_control, 'mseucon\Natalia_Nast', :applies_to_children => true, :applies_to_self => true
  rights    :full_control, 'root', :applies_to_children => true, :applies_to_self => true
  rights    :full_control, 'Benutzer', :applies_to_children => true, :applies_to_self => true
  inherits   true
  not_if    "sc query MSSQL$SQLEXPRESS"
  #do ::File.exists?("#{distrib_path}/SQL_Server_2008_R2_SP2_Express_Edition/setup.exe") end
end

#remote_directory "#{distrib_path}/PSTools" do 
#  source "PSTools"
#  action    :create
#  rights    :full_control, 'Jeder', :applies_to_children => true, :applies_to_self => true
#  rights    :full_control, 'mseucon\Natalia_Nast', :applies_to_children => true, :applies_to_self => true
#  rights    :full_control, 'root', :applies_to_children => true, :applies_to_self => true
#  rights    :full_control, 'Benutzer', :applies_to_children => true, :applies_to_self => true
#  inherits   true
#  not_if    "sc query MSSQL$SQLEXPRESS"
  #do ::File.exists?("#{distrib_path}/PSTools") end
#end

#Chef::Log.info(node["machinename"])

#case node["machinename"]
#when "EIS-MS-DB-71"
#  user = "mseucon\\services-db-71" 
#  password = 
#when "EIS-MS-SE-1"
#  user = "mseucon\\ServicesSE"
#  password = "SE-2015eu"
#end  
#end


batch "sqlexpr_install" do
  code <<-EOH
  cmd /C c:\\sqlexpress\\SQL_Server_2008_R2_SP2_Express_Edition\\Install_Server.bat
  EOH
  not_if "sc query MSSQL$SQLEXPRESS"
end

#remote_directory "#{distrib_path}/PSTools" do 
#  source "PSTools"
#  action    :delete
#  only_if "sc query MSSQL$SQLEXPRESS"
#end

remote_directory "#{distrib_path}/SQL_Server_2008_R2_SP2_Express_Edition" do 
  source "SQL_Server_2008_R2_SP2_Express_Edition"
  action    :delete
  only_if "sc query MSSQL$SQLEXPRESS"
end

directory distrib_path do
  action    :delete
  only_if "sc query MSSQL$SQLEXPRESS"
end



