#
# Cookbook Name:: java
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

distrib_path = "C:/java"

directory distrib_path do
  action    :create
  rights    :full_control, 'Jeder', :applies_to_children => true, :applies_to_self => true
#  not_if    "java -version"
end


%w{ jdk-8u60-windows-x64.exe setup.bat}.each do |file|
	cookbook_file file do
#	  backup 5
	  action :create
	  path "#{distrib_path}/#{file}"
	  rights :read, 'Jeder', :applies_to_children => true, :applies_to_self => true
#	  rights :full_control, node[:eucon][:user], :applies_to_children => true, :applies_to_self => true#
#	  rights :full_control, node[:eucon][:group], :applies_to_children => true, :applies_to_self => true
#	  inherits true
	end
end

batch "sqlexpr_install" do
  code <<-EOH
  cmd /C c:\\java\\setup.bat
  EOH
end

#directory distrib_path do
#  action  :delete
#  only_if "java -version"
#end
