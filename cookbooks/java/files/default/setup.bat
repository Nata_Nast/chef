pushd %~dp0 

jdk-8u60-windows-x64.exe /s 
rem /L c:\chef\chef-starter_db-152\chef-repo\cookbooks\java\setup.log
rem Set the JAVA_HOME environment variable and insert it into the system path.
rem This will make the javac and java commands reachable from the command line.
setx -m JAVA_HOME "C:\Program Files\Java\jdk1.8.0_60"
setx -m PATH "%JAVA_HOME%\bin;%PATH%"

popd